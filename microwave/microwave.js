// variable that holds the timer
var timer;
// varible that holds the display
var display = [0,0,":",0,0];
// minutes
var mCounter = 0;
// seconds
var sCounter = 0;
// counter that represents the index in the array
var counter = 0;

$(function() {
  //display will equal elements of array, join("") joins them without anything
  //in between them
  $("#txtNumber").val(display.join(""));

  // function of when the any of the number and sign buttons is clicked
  // it gets added in the array and displays it
  $("button[id^='button']").click(function() {
    // variable that holds the value of the buttoned clicked
    var val = $(this).val();
    // if the counter is less than the array length,
    // button value is being added at the array
    if(counter < 5) {
        // skip the element at array index 2
        if(counter == 2) {
        // ":" will remain constant
        display[2] == ":";
        // move to the next array index
        counter++;
        // and add this value to the array
        display[counter] = val;
      } else {
        // add this value to the array
        display[counter] = val;
      }
    }
    // increment array index counter to keep adding to the next available
    // index
    counter++;
    // display to screen
    $("#txtNumber").val(display.join(""));
  });

  // function when the start button is clicked
  $("#Start").click(function() {
    // call startTimer() function
    startTimer();
  });

  $("#Stop").click(function() {
    clearTimeout(timer);
    console.log(display);
  });

//function will clear display, but will proceed to keep inputting to the right
//after clearing?
  $("#Clear").click(function() {
    display = [0,0,":",0,0];
    clearTimeout(timer);
    counter = 0;
    $("#txtNumber").val(display.join(""));
    location.reload();
  });
});

//makes the textbox unclickable, keypad inputs only!
$('#txtNumber').focus(function() {
  $(this).blur();
});

//function for beep noise
function beeper() {
  var audio = document.getElementById('beep')
  audio.play();
}

// function that starts the timer
function startTimer() {
  // get the minute
  mCounter = display.slice(0, 2).join("");
  // get seconds
  sCounter = display.slice(-2).join("");

  // parse the minutes and seconds to int
  var min = parseInt(mCounter);
  var sec = parseInt(sCounter);

  // count down the seconds
  sec--;

  // if seconds and minutes are either < 0, stop the timer, else keep counting
  // down
  if(min <= 0 && sec <= 0) {
    clearTimeout(timer);
  }else if(sec <= 0 && min >= 0) {
    min--;
    sec = 60;
    timer = setTimeout("startTimer()",1000);
  }else {
    timer = setTimeout("startTimer()",1000);
  }

  // display the count down again after each second
  // but parse them back to string again
  var m = String(min);
  var s = String(sec);


  var mTemp = m.split("");
  var sTemp = s.split("");
  // empty out the display again for a brand new array
  display = [];
  // if minute or second is < 10 then show it as 09, 08 ...
  if(mTemp.length == 2) {
    display.push(mTemp[0]);
    display.push(mTemp[1]);
  }else {
    display.push("0");
    display.push(mTemp[0]);
  }
  display.push(":");
  if(sTemp.length == 2) {
    display.push(sTemp[0]);
    display.push(sTemp[1]);
  }else {
    display.push("0");
    display.push(sTemp[0]);
  }
  console.log(display);
  $("#txtNumber").val(display.join(""));
};

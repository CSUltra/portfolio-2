// holds the variable being displayed
var display = "";

// function that adds the value of the button to the display
function addNumber(buttonElement) {
   display += document.getElementById(buttonElement.id).value;
   document.getElementById('txtNumber').value = display;
}

//Quick use of an eval function to make the coding easy!
function calculate() {
   //console.log(display)
   document.getElementById('txtNumber').value = eval(display);
   // clear display again for a new calculation
   display = "";
}

// function that clears the display on clicked
function clearBtn() {
   document.getElementById('txtNumber').value = "";
   display = "";
}

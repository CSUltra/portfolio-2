var gamePieces = ["Rock","Paper","Scissors","Dynamite"]

var compChoiceInitial = gamePieces[Math.floor(Math.random() * gamePieces.length)];
var compChoice = getRandomGamePiece();
//initial prompt to make a choice:
var playerChoiceInitial = prompt("Rock, Paper, Scissors, Dynamite! Choose your champion!")
//as long as the user inputs the same characters, capitalization won't matter:
var playerChoice = playerChoiceInitial.toLowerCase();

function getRandomGamePiece(gamePiecesLength){
    //get random pieces from an array for the computer's choice
    compChoiceInitial = gamePieces[Math.floor(Math.random() * gamePieces.length)].toLowerCase();
    return compChoiceInitial;
}

//whoWins function for the sake of comparing outcomes
function whoWins(){
    document.getElementById("playerPick").innerHTML = playerChoice;
    document.getElementById("computerPick").innerHTML = compChoice;
    if(playerChoice == compChoice){
        //simple jQuery to display results
        $('#results').text("Tie! At least comfort yourself in the knowledge that you "
    + "essentially think like a computer.")
    }else if(
        playerChoice == "rock" && compChoice == "scissors" ||
        playerChoice == "dynamite" && compChoice == "rock" ||
        playerChoice == "paper" && compChoice == "rock" ||
        playerChoice == "scissors" && compChoice == "paper" ||
        playerChoice == "scissors" && compChoice == "dynamite" ||
        playerChoice == "dynamite" && compChoice == "paper"){

            $('#results').text("Your gracious victory in this simple game of Rock/Paper/Scissors/Dynamite" +
        " has granted you substantial and unyielding powers of mysterious, forgotten deities. Congratulations!")
        }else{
        //user will lose if they pick anything other than the given items, too.
        //it's not technically validation, but who wants to lose by default?
        //nobody, I tells ya'.
            $('#results').text("Sorry! Your friends have been gunned down off the coast of Turkey, a hoard of" +
        " undead has kidnapped your dog, your bathtub now only leaks spiders out of its faucet, and the milk in your" +
        " fridge has gone sour. And all of this could have been prevented if you just picked the winning option!")
        }
    }

//Refreshes the page after the first input, restarting the game. I'm not sure
//if it was exactly what you were looking for, Perry, but I tried scrambling the
//code around a few times and couldn't quite figure out how to initialize everything
//with the direct click!
function gameStart(){
    location.reload(true);
}


//Goldfinger
//he's the man
//the man with the Midas touch
//a spider's touch

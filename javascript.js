
// // * * * * * * * * * * * * * * * * * * * * * * * * * * *
// // 1. Get HTML Elements
// // * * * * * * * * * * * * * * * * * * * * * * * * * * *
var openButton      = document.querySelector('#open-mobile-menu-button');
// var openButton = document.getElementById('open-mobile-menu-button');
var closeButton     = document.querySelector('#close-mobile-menu-button');
// var closeButton     = document.getElementById('close-mobile-menu-button');
var mobileMenu      = document.querySelector('#mobile-menu');
//
//
//
// // * * * * * * * * * * * * * * * * * * * * * * * * * * *
// // 2. Define Abilities (Functions)
// // * * * * * * * * * * * * * * * * * * * * * * * * * * *
function openMenu() {
  mobileMenu.className = 'menu open';
}

function closeMenu() {
  mobileMenu.className = 'menu close';
}
//
//
//
// // * * * * * * * * * * * * * * * * * * * * * * * * * * *
// // 3. Wire everything up
// // * * * * * * * * * * * * * * * * * * * * * * * * * * *
openButton.addEventListener('click', openMenu);
closeButton.addEventListener('click', closeMenu);
//
// /* * * * * * * * * * * * * * * Flashy Computer * * * * * * * * * * * * * * * */
//

$(document).ready(function(){
  //prepare Your data array with img urls
  var compuArray=new Array();
  compuArray[0]="./images/computer1.png";
  compuArray[1]="./images/computer2.png";

  //start with id=0 after 5 seconds
  var thisId=0;

  window.setInterval(function(){
    $('#compuId').attr('src',compuArray[thisId]);
    thisId++; //increment compu array id
    if (thisId==2) thisId=0; //repeat from start
  },1000);

  var emote = ["ƪ(˘⌣˘)ʃ","(ಠ ∩ಠ)", "(ಥ◡ಥ)", "(◉Θ◉)","(⊙_ʘ)","(✖‿✖)","(・∇・)",
  "(⊙ꇴ⊙)","(ง •̀_•́)ง","くコ:彡","( ͡° ͜ʖ ͡°)","(ﾟ⊿ﾟ)","(๑ ิټ ิ)",
  "(˘̭⺫˘̭ ;)","(・ε・)"];
  var counter = 0;
  var elem = document.getElementById("flashyComp");
  setInterval(change, 1000);

  function change() {
    elem.innerHTML = emote[counter];
    counter++;
    if (counter >= emote.length) {
      counter = 0;
    }
  }
});


/* * * * * * * * * * * *  Accordion * * * * * * * * * * * * * * */

$( function() {
  $( "#accordion" ).accordion();
} );

/* * * *  * * * * * * * Blink Cloud * * * * * * * * * * * * * * */
